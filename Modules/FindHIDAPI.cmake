# Cache Variables: (probably not for direct use in your scripts)
#  HIDAPI_INCLUDE_DIR
#  HIDAPI_LIBRARY
#
# Non-cache variables you might use in your FindQMI.cmake:
#  HIDAPI_FOUND
#  HIDAPI_INCLUDE_DIRS
#  HIDAPI_LIBRARIES
#
# Requires these CMake modules:
#  FindPackageHandleStandardArgs (known included with CMake >=2.6.2)

find_library(HIDAPI_LIBRARY
        NAMES hidapi hidapi-libusb)

find_path(HIDAPI_INCLUDE_DIR
        NAMES hidapi.h
        PATH_SUFFIXES
        hidapi)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(HIDAPI
        DEFAULT_MSG
        HIDAPI_LIBRARY
        HIDAPI_INCLUDE_DIR)

if(HIDAPI_FOUND)
    set(HIDAPI_LIBRARIES "${HIDAPI_LIBRARY}")

    set(HIDAPI_INCLUDE_DIRS "${HIDAPI_INCLUDE_DIR}")
endif()

mark_as_advanced(HIDAPI_INCLUDE_DIR HIDAPI_LIBRARY)