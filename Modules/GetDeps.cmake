include(get_cpm)

macro(download_dependency DEP_NAME DEP_VERSION)
    CPMAddPackage(
        NAME ${DEP_NAME}
        GIT_REPOSITORY http://gitlab.hendergraves.uk/siemens/libraries/${DEP_NAME}.git
        GIT_TAG ${DEP_VERSION}
    )
endmacro(download_dependency DEP_NAME DEP_VERSION)
